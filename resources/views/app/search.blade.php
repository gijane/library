@extends('app.layouts.layout')

@section('title') Search by title @endsection

@section('content')

    <form action="{{route('search')}}" method="get" class="editor editor-search">
        <p>
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control" required>
        </p>
        <p>
            <button class="floating-button">Search</button>
        </p>
    </form>

    @foreach($book as $el)
        <div class="alert-info">
            <p>
                <h3>{{ $el->title }}</h3>
            </p>
            <p>
                <img src="{{ asset('uploads/books/' . $el->image) }}" width="200px">
            </p>
            <p>
                Author: {{ $el->author }}
            </p>
            <p>
                Year: {{ $el->year }}
            </p>
        </div>
    @endforeach
@endsection

