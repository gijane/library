@extends('app.layouts.layout')

@section('title') Order a book @endsection

@section('content')

    <form action="{{ route('add-reader-form') }}" method="post" class="editor">

        @csrf

        <p>
            <label for="reader">Your name:</label>
            <input type="text" name="name" id="reader" class="form-control" required>
        </p>

        <p>
            <label for="email">Your e-mail:</label>
            <input type="email" name="email" id="email" class="form-control" required>
        </p>

        <p>
            <label for="book">Choose a book:</label>
            <select name="book_id" id="book" class="form-control">

                @foreach($data as $book)

                    <option value="{{ $book->book_id }}">
                        {{ $book->title }}
                    </option>

                @endforeach

            </select>
        </p>



        <p>
            <button type="submit" class="floating-button">Submit</button>
        </p>

    </form>

@endsection
