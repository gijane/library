@extends('app.layouts.layout')

@section('title') Books @endsection

@section('content')

    <div class="dropdown">
        <button class="dropbtn">Sort by room</button>
            <div class="dropdown-content">
                @foreach($rooms as $room)
                    <a href="{{route('sort', ['id'=>$room->room_id])}}">{{ $room->name }}</a>
                @endforeach
            </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col" class="id">ID</th>
                <th scope="col"></th>
                <th scope="col">Title</th>
                <th scope="col">Author</th>
                <th scope="col">Year</th>
                <th scope="col">Room</th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $element)
                <tr>
                    <th scope="row" class="id">{{ $element->book_id}}</th>
                    <td><img src="{{ asset('uploads/books/' . $element->image) }}" width="200px"></td>
                    <td>{{ $element->title}}</td>
                    <td>{{ $element->author}}</td>
                    <td>{{ $element->year}}</td>
                    <td>{{ $element->room}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

