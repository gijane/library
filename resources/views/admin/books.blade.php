@extends('admin.layouts.layout')

@section('title') Books @endsection

@section('content')
    <p>
    <table class="table-wide">
        <thead>
        <tr>
            <th scope="col" class="id">ID</th>
            <th scope="col"></th>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
            <th scope="col">Year</th>
            <th scope="col">Room</th>
            <th scope="col">Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach($data as $element)
            <tr>
                <th scope="row" class="id">{{ $element->book_id}}</th>
                <td><img src="{{ asset('uploads/books/' . $element->image) }}" width="200px"></td>
                <td>{{ $element->title}}</td>
                <td>{{ $element->author}}</td>
                <td>{{ $element->year}}</td>
                <td>{{ $element->room}}</td>
                <td>
                    <a href="/admin/books/{{ $element->book_id}}/edit">
                        <button type="submit" class="floating-button floating-button-yellow">Edit</button>
                    </a>
                    <br/>
                    <br/>
                    <form action="/admin/books/{{ $element->book_id}}" method="post">
                        @method('DELETE')
                        @csrf
                        <button class="floating-button floating-button-red">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </p>
@endsection
