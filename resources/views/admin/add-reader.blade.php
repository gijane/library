@extends('admin.layouts.layout')

@section('title') Add reader @endsection

@section('content')

    <form action="/admin/readers" class="editor editor-add-reader" method="post">
        @csrf
        <p>
            <label for="reader">Name:</label>
            <input type="text" name="name" id="reader" class="form-control" required>
        </p>

        <p>
            <label for="email">Email:</label>
            <input type="text" name="email" id="email" class="form-control" required>
        </p>

        <p>
            <label for="book">Book title:</label>
            <select name="book_id" id="book" class="form-control">
                @foreach($books as $book)
                    <option value="{{ $book->book_id }}">
                        {{ $book->title }}
                    </option>
                @endforeach
            </select>
        </p>

        <p>
            <button type="submit" class="floating-button">Add</button>
        </p>

    </form>

@endsection
