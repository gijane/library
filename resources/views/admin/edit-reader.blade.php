@extends('admin.layouts.layout')

@section('title') Edit reader @endsection

@section('content')

    <form action="/admin/readers/{{ $reader->reader_id }}" class="editor editor-edit-reader" method="post">
        @csrf
        @method('PUT')
        <p>
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" value="{{ $reader->name }}" class="form-control" required>
        </p>

        <p>
            <label for="email">Email:</label>
            <input type="text" name="email" id="email" value="{{ $reader->email }}" class="form-control" required>
        </p>

        <p>
            <label for="book">Book title:</label>
            <select name="book_id" id="book" class="form-control">
                @foreach($books as $book)
                    <option value="{{ $book->book_id}}">
                        {{ $book->title }}
                    </option>
                @endforeach
            </select>
        </p>

        <p>
            <button type="submit" class="floating-button">Edit</button>
        </p>

    </form>
@endsection
