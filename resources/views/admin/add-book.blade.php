@extends('admin.layouts.layout')

@section('title') Add a book @endsection

@section('content')

        <form action="/admin/books" method="post" class="editor editor-add" enctype="multipart/form-data">
            @csrf
            <p>
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control" required>
            </p>
            <p>
                <label for="author">Author:</label>
                <input type="text" name="author" id="author" class="form-control" required>
            </p>
            <p>
                <label for="year">Year:</label>
                <input type="text" name="year" id="year" class="form-control" required>
            </p>
            <p>
                <label for="room">Room:</label>
                <select name="room_id" id="room" class="form-control">
                    @foreach($rooms as $room)
                        <option value="{{ $room->room_id}}">
                            {{ $room->name }}
                        </option>
                    @endforeach
                </select>
            </p>
            <p>
                <label for="image">Photo:</label>
                <input type="file" name="image" id="image" required>
            </p>
            <p>
                <button type="submit" class="floating-button">Add</button>
            </p>
        </form>
@endsection
