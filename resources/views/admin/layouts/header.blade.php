<nav class="nav">
    <a class="nav-a" href="/admin/books">BOOKS</a>
    <a class="nav-a" href="/admin/books/create">ADD BOOK</a>
    <a class="nav-a" href="/admin/readers">READERS</a>
    <a class="nav-a" href="/admin/readers/create">ADD READER</a>
    @guest
        @if (Route::has('login'))
            <a class="nav-a" href="{{ route('login') }}">{{ __('Login') }}</a>
        @endif

    @else
        <a id="navbarDropdown" class="nav-a" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }}
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="nav-a" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    @endguest
</nav>
