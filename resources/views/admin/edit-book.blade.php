@extends('admin.layouts.layout')

@section('title') Edit book @endsection

@section('content')

    <form action="/admin/books/{{ $book->book_id }}" class="editor editor-edit" enctype="multipart/form-data" method="post">
        @csrf
        @method('PUT')

        <img src="{{ asset('uploads/books/' . $book->image) }}" width="200px">

        <p>
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control" value="{{ $book->title }}" required>
        </p>

        <p>
            <label for="author">Author:</label>
            <input type="text" name="author" id="author" class="form-control" value="{{ $book->author }}" required>
        </p>

        <p>
            <label for="year">Year:</label>
            <input type="text" name="year" id="year" class="form-control" value="{{ $book->year }}" required>
        </p>

        <p>
            <label for="room">Room:</label>
            <select name="room_id" id="room" class="form-control">
                @foreach($rooms as $room)
                    <option value="{{ $room->room_id}}">
                        {{ $room->name }}
                    </option>
                @endforeach
            </select>
        </p>

        <p>
            <label for="image">Photo:</label>
            <input type="file" name="image" id="image" required>
        </p>

        <p>
            <button type="submit" class="floating-button">Edit</button>
        </p>

    </form>
@endsection
