@extends('admin.layouts.layout')

@section('title') Readers @endsection

@section('content')

    <table class="table-wide">

        <thead>
        <tr>
            <th scope="col" class="id">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Book title</th>
            <th scope="col">Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach($data as $element)
            <tr>
                <th scope="row" class="id">{{ $element->reader_id}}</th>
                <td>{{ $element->reader}}</td>
                <td>{{ $element->email}}</td>
                <td>{{ $element->title}}</td>
                <td>
                    <a href="/admin/readers/{{ $element->reader_id}}/edit">
                        <button type="submit" class="floating-button floating-button-yellow">Edit</button>
                    </a>
                    <br/>
                    <br/>
                    <form action="/admin/readers/{{ $element->reader_id}}" method="post">
                        @method('DELETE')
                        @csrf
                        <button class="floating-button floating-button-red">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@endsection
