@extends('admin.layouts.layout')

@section('content')
<div class="editor editor-dashboard">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}

</div>
@endsection
