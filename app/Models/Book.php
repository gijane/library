<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Book extends Model {

    use HasFactory;
    protected $table = 'books';
    protected $primaryKey = 'book_id';
    protected $fillable = ['title', 'author', 'year', 'image', 'room_id'];
    public $timestamps = false;

    public static function allBooks() {
        return DB::table('books')->get();
    }
}
