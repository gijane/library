<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller {

    public function allBooks() {
        $book = DB::table('books as b')
            ->select('b.book_id',
                'b.title as title',
                'b.image as image',
                'b.author as author',
                'b.year as year',
                'ro.name as room')
            ->join('rooms as ro', 'b.room_id', '=', 'ro.room_id')
            ->get();
        return view('app.books', ['data' => $book, 'rooms' => Room::allRooms()]);
    }

    public function searchBook(Request $request) {
        $title = $request->input('title');
        $book = DB::table('books')
            ->select('*')
            ->where('title', '=', $title)->get();
        return view('app.search',['book' => $book]);
    }

    public function books() {
        return view('app.add-reader',  ['data' => Book::all()]);
    }

    public function sortBooks($id) {
        $book = DB::table('books as b')
            ->select('b.book_id',
                'b.title as title',
                'b.image as image',
                'b.author as author',
                'b.year as year',
                'ro.name as room',
                'b.room_id as room_id',
                'ro.room_id as ro_id'
            )
            ->join('rooms as ro', 'b.room_id', '=', 'ro.room_id')
            ->where('b.room_id', '=', $id)
            ->get();
        return view('app.books', ['data' => $book, 'rooms' => Room::allRooms()]);
    }
}

