<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Room;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class AdminBookController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index() {

        $book = DB::table('books as b')
            ->select(
                'b.book_id',
                'b.title as title',
                'b.author as author',
                'b.image as image',
                'b.year as year',
                'ro.name as room')
            ->join(
                'rooms as ro',
                'b.room_id',
                '=',
                'ro.room_id')
            ->get();

        return view('admin.books', ['data' => $book]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create() {
        return view('admin.add-book', ['rooms' => Room::allRooms()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Request|Redirector
     */
    public function store(Request $request) {

        $book = new Book();

        $book->title = $request->input('title');
        $book->year = $request->input('year');
        $book->author = $request->input('author');
        $book->room_id = $request->input('room_id');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/books/', $filename);
            $book->image = $filename;
        } else {
            $book->image = '';
            return $request;
        }

        $book->save();

        return redirect('admin/books')->with('success', 'The book was added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id) {

        $book = Book::where('book_id', $id)->first();

        return view('admin.edit-book', ['book'=>$book, 'rooms' => Room::allRooms()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Application|RedirectResponse|Request|Redirector
     */
    public function update(Request $request, $id) {
        $book = Book::where('book_id', $id)->first();

        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->year = $request->input('year');
        $book->room_id = $request->input('room_id');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/books/', $filename);
            $book->image = $filename;
        } else {
            $book->image = '';
            return $request;
        }

        $book->save();

        return redirect('admin/books')->with('success', 'The book was edited successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy($id) {

        Book::destroy($id);

        return redirect('admin/books')->with('success', 'The book was deleted successfully!');
    }
}
