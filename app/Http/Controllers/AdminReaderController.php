<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Reader;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

class AdminReaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index() {

        $reader = DB::table('readers as r')
            ->select(
                'r.reader_id',
                'b.title as title',
                'r.name as reader',
                'r.email as email')
            ->join(
                'books as b',
                'b.book_id',
                '=',
                'r.book_id')
            ->get();

        return view('admin.readers', ['data' => $reader]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create() {
        return view('admin.add-reader', ['books' => Book::allBooks()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request) {

        $reader = new Reader();

        $reader->name = $request->input('name');
        $reader->book_id = $request->input('book_id');
        $reader->email = $request->input('email');

        $reader->save();

        return redirect('admin/readers')->with('success', 'The reader was added successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id) {

        $reader = Reader::where('reader_id', $id)->first();

        return view('admin.edit-reader', ['reader'=>$reader, 'books' => Book::allBooks()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Application|Redirector|RedirectResponse
     */
    public function update(Request $request, $id) {

        $reader = Reader::where('reader_id', $id)->first();

        $reader->name = $request->input('name');
        $reader->book_id = $request->input('book_id');
        $reader->email = $request->input('email');

        $reader->save();

        return redirect('admin/readers')->with('success', 'The reader was edited successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy($id) {

        Reader::destroy($id);

        return redirect('admin/readers')->with('success', 'The reader was deleted successfully!');
    }
}
