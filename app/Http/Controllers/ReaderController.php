<?php

namespace App\Http\Controllers;
use App\Models\Reader;
use Illuminate\Http\Request;

class ReaderController extends Controller {

    public function createReader(Request $request) {
        $reader = new Reader();
        $reader->name = $request->input('name');
        $reader->email = $request->input('email');
        $reader->book_id = $request->input('book_id');

        $reader->save();

        return redirect()->route('guesthome')->with('success', 'Your order passed successfully!');


    }
}
