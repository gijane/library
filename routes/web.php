<?php

use App\Http\Controllers\AdminBookController;
use App\Http\Controllers\AdminReaderController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app/home');
})->name('guesthome');

Route::get(
    '/books',
    'App\Http\Controllers\BookController@allBooks'
)->name('books');

Route::get(
    '/room/{id}',
    'App\Http\Controllers\BookController@sortBooks'
)->name('sort');

Route::get(
    '/add-reader',
    'App\Http\Controllers\BookController@books'
)->name('add-reader');

Route::post(
    '/add-reader/submit',
    'App\Http\Controllers\ReaderController@createReader'
)->name('add-reader-form');

Route::get(
    '/search',
    'App\Http\Controllers\BookController@searchBook'
)->name('search');

//admin - resource controllers
Route::resource(
    'admin/books',
    AdminBookController::class
);

Route::resource(
    'admin/readers',
    AdminReaderController::class
);

Auth::routes([
    'register' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
